#/bin/bash

# script is intended to populate ldap directory with test data. Update for other services.
# tested under Cygwin with open LDAP package

LDAP_HOST='10.9.8.160'
LDAP_PORT=10389

# add sms-proxy ldap test data
ldapmodify -h $LDAP_HOST -p $LDAP_PORT -x -c -f ./config/sms-proxy-ldap-data.ldif