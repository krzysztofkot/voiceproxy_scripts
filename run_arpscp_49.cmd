@ECHO OFF

call setup_env.bat

%SIMUL_HOME% -l arpscp_49_simulator.log -f config/loopback/setup-arpscp-49.commands -f config/setup-eureg3-scenarios-arpscp-49.commands -f scens\mo-proxy\postpaid-arpscp-49.properties
