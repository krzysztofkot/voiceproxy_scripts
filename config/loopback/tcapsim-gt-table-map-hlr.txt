# 1 AddressType Address PC SSN NatureOfAddress
# 2 AddressType Address PC SSN TranslationType
# 3 AddressType Address PC SSN TranslationType NumberingPlan
# 4 AddressType Address PC SSN TranslationType NumberingPlan NatureOfAddress
#
# A match will only be returned if the leading digits of the address and all other parameters
# match the rule.  In the case of multiple matching rules, the rule with the most address
# digits matching will be returned (I.e. most significant address match).
#
# Examples:
# 1 C7 123456 2 221 1
# 1 C7 1234     2 221 1
# 2 C7 98131   1 232 122
# 3 A7 12345   1-1-1 221 42 1
# 4 C7 8193129 4 200 89 1 1


# Rhino
4 C7 41000000001 1 32 0 1 4
4 C7 51000000001 1 32 0 1 4

# HLR
4 C7 316540890001 1 6 0 1 4
4 C7 316540055832 1 6 0 1 4