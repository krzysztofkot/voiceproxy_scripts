
  MMS Proxy execution release notes
  ---------------------------------
  
  Test cases are divided to three parts as below. it is done due to need special configuration for test cases MMS_07 and MMS _08.
  They have to have simulate special configuration to simulate required behaviour
  
   - Execution test cases MMS_01 to MMS_05 need to start up simulator for MMSC and OCS, configuration MMSC is included at mmsc.properties file,
     configuration for OCS is included at ocs1.properties. Before execution test cases set there should be started up OSC simulator script 'setup-ocs1.commands'.
	 test cases MMS_01 to MMS_05 are executed running simulator scripts 'setup-mmsc_tc1-5.commands'.
	 
   - Execution test MMS_07 need to down communication to LDAP repository to simulate internal error. Another test cases cannot be executed for this case.	 
 
   - Execution test MMS_08 need to down OCS simulator to simulate OCS unavailability. Another test cases cannot be executed for this case.