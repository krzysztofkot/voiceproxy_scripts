:: This is main script to run sms-proxy functional tests session using OC scenario simulator tool
@echo off
call setup_env.bat
call %SIMUL_HOME%\scenario-simulator.bat -l smsc.log -f config\setup-smsc.commands -f scens\sms-proxy\run-scens.commands
echo.
echo **************************************************
echo.
echo Basic scenarios ended
echo Disable OCS server connection before continuation
echo.
echo **************************************************
echo.
pause
call %SIMUL_HOME%\scenario-simulator.bat -l smsc.log -f config\setup-smsc.commands -f scens\sms-proxy\run-ocs-down-scens.commands
echo.
echo **************************************************
echo.
echo OCS down scenarios ended
echo Disable LDAP server connection before continuation
echo.
echo **************************************************
echo.
pause
call %SIMUL_HOME%\scenario-simulator.bat -l smsc.log -f config\setup-smsc.commands -f scens\sms-proxy\run-ldap-down-scens.commands
echo.
echo **************************************************
echo. 
echo LDAP down scenarios ended 
echo Test session ended
echo.
echo **************************************************
echo.
pause



run-ldap-down-scens.commands