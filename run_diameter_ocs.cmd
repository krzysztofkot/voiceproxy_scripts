:: This is script that shall be used to run OCS simulator for all diameter services.
:: config\setup-diam-ocs.commands file must be configured for all proxies

call setup_env.bat

start %SIMUL_HOME%\scenario-simulator.bat -l diam-ocs.log -f config\setup-diam-ocs.commands