#/bin/bash

# I use this under Cygwin to see scenarios results from simulator log. Must be executed before starting client-side simulator.

LOGS='/cygdrive/e/OpenCloud/scenario-simulator-2.3-eureg/scenario-simulator-2.3.0.2/logs'


if [ $# -lt 1 ]
then
	i=1
	files=(`ls $LOGS/`)
	for file in ${files[*]}
	do echo $i') '$file
	let i++
	done
	echo Select file from which to read results:
	read choice
	LOG_FILE=${files[`let choice-1`]}
else
	LOG_FILE=$1
fi



tail -f $LOGS/$LOG_FILE | sed -n -e 's/.*Started a "\(.*\)".*/Session: \1/p' -e 's/.*\(server\|client\).*Session ended: Matched scenario definition.*/\t \1: OK/p' -e 's/.*\(server\|client\).*Session ended: Session was not matched.*/\t \1: FAILED/p'