@ECHO OFF

call setup_env.bat

%SIMUL_HOME% -l msc_simulator.log -f config/loopback/setup-msc-mo-relay.commands -f config/setup-eureg3-scenarios-msc-mo-relay.commands -f scens/mo-proxy/msc-mo-relay.properties
